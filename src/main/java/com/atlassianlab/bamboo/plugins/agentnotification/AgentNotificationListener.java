package com.atlassianlab.bamboo.plugins.agentnotification;

import com.atlassian.bamboo.event.agent.AgentRegisteredEvent;
import com.atlassian.bamboo.notification.NotificationDispatcher;
import com.atlassian.bamboo.notification.NotificationManager;
import com.atlassian.bamboo.notification.NotificationRecipient;
import com.atlassian.bamboo.notification.NotificationRule;
import com.atlassian.bamboo.notification.NotificationType;
import com.atlassian.bamboo.notification.SystemNotificationService;
import com.atlassian.bamboo.utils.BambooNotificationUtils;
import com.atlassian.bamboo.v2.build.events.AgentOfflineEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.annotations.VisibleForTesting;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import static com.atlassianlab.bamboo.plugins.agentnotification.Keys.OFFLINE_NOTIFICATION_PLUGIN_KEY;
import static com.atlassianlab.bamboo.plugins.agentnotification.Keys.ONLINE_NOTIFICATION_PLUGIN_KEY;

public class AgentNotificationListener {
    private static final Logger log = Logger.getLogger(AgentNotificationListener.class);

    private final SystemNotificationService systemNotificationService;
    private final NotificationManager notificationManager;
    private final NotificationDispatcher notificationDispatcher;

    @Inject
    public AgentNotificationListener(@ComponentImport final SystemNotificationService systemNotificationService,
                                     @ComponentImport final NotificationManager notificationManager,
                                     @ComponentImport final NotificationDispatcher notificationDispatcher) {
        this.systemNotificationService = systemNotificationService;
        this.notificationManager = notificationManager;
        this.notificationDispatcher = notificationDispatcher;
    }

    @EventListener
    public void onAgentOnlineHandler(AgentRegisteredEvent event) {
        log.debug("Agent is online: " + event.getAgent());
        for (NotificationRule notificationRule : systemNotificationService.getSystemNotificationRules()) {
            handleEvent(notificationRule,
                    ONLINE_NOTIFICATION_PLUGIN_KEY, AgentOnlineNotification.class, event);
        }
    }

    @EventListener
    public void onAgentOfflineHandler(AgentOfflineEvent event) {
        log.debug("Agent is offline: " + event.getBuildAgent());
        for (NotificationRule notificationRule : systemNotificationService.getSystemNotificationRules()) {
            handleEvent(notificationRule,
                    OFFLINE_NOTIFICATION_PLUGIN_KEY, AgentOfflineNotification.class, event);
        }
    }

    private <T extends AgentChangeStatusNotification> void handleEvent(final NotificationRule notificationRule,
                                                                       @NotNull final String key,
                                                                       final Class<T> notificationClass,
                                                                       final Object event) {
        if (key.equals(notificationRule.getConditionKey())) {
            final NotificationType notificationType = notificationManager.getNotificationType(notificationRule);
            if (notificationType == null
                    || !notificationType.isNotificationRequired(event)) {
                log.debug("Notification is not required");
                return;
            }
            log.debug("Agent change status rule found for recipient: " + notificationRule.getRecipient() + ", type: " + notificationRule.getRecipientType());
            final NotificationRecipient recipient = notificationManager.getNotificationRecipient(notificationRule);

            if (recipient != null) {
                final T notification = createNotification(notificationClass);

                notification.addRecipient(recipient);

                notification.setEvent(event);
                notificationDispatcher.dispatchNotifications(notification);
            }
        }
    }

    @VisibleForTesting
    @NotNull
    protected <T extends AgentChangeStatusNotification> T createNotification(Class<T> notificationClass) {
        return BambooNotificationUtils.createNotification(notificationClass);
    }
}