package com.atlassianlab.bamboo.plugins.agentnotification;

import com.atlassian.bamboo.filter.Pager;
import com.atlassian.bamboo.resultsummary.AgentResultsSummaryManager;
import com.atlassian.bamboo.resultsummary.BuildResultsSummary;
import com.atlassian.bamboo.util.Narrow;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.v2.build.events.AgentOfflineEvent;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import java.util.EventObject;
import java.util.List;
import java.util.Map;

public class AgentOfflineNotification extends AgentChangeStatusNotification {
    private static final String TEXT_EMAIL_TEMPLATE = "notification-templates/AgentWentOfflineTextEmail.ftl";
    private static final String HTML_EMAIL_TEMPLATE = "notification-templates/AgentWentOfflineHtmlEmail.ftl";
    private static final String IM_TEMPLATE = "notification-templates/AgentWentOfflineTextIm.ftl";
    private static final String HTML_IM_TEMPLATE = "notification-templates/AgentWentOfflineHtmlIm.ftl";
    private static final int PAGE_SIZE = 3;

    @Inject
    @ComponentImport
    private AgentResultsSummaryManager agentResultsSummaryManager;

    @Override
    protected String getTextImTemplate() {
        return IM_TEMPLATE;
    }

    @Override
    protected String getHtmlImTemplate() {
        return HTML_IM_TEMPLATE;
    }

    @Override
    protected String getHtmlEmailTemplate() {
        return HTML_EMAIL_TEMPLATE;
    }

    @Override
    protected String getTextEmailTemplate() {
        return TEXT_EMAIL_TEMPLATE;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Agent offline notification";
    }

    @Override
    public String getEmailSubject() {
        final AgentOfflineEvent event = Narrow.downTo(getPojoEvent(), AgentOfflineEvent.class);
        if (event != null) {
            return getText("agent.notification.offline.email.subject", event.getBuildAgent().getName());
        }
        return StringUtils.EMPTY;
    }

    @Override
    protected <T extends EventObject> void populateAgentDetails(Map<String, Object> context, T event) {
        if (event instanceof AgentOfflineEvent) {
            final AgentOfflineEvent offlineEvent = (AgentOfflineEvent) event;
            final BuildAgent buildAgent = offlineEvent.getBuildAgent();
            final List<BuildResultsSummary> builds = agentResultsSummaryManager.getLatestSummariesForAgent(buildAgent.getId(), PAGE_SIZE);
            final Pager<BuildResultsSummary> buildsPager = new Pager<>(PAGE_SIZE);
            buildsPager.setPageFromList(builds);

            context.put(CFG_AGENT, new Agent(buildAgent.getId(), buildAgent.getName()));
            context.put("buildsPager", buildsPager);
        }
    }
}
