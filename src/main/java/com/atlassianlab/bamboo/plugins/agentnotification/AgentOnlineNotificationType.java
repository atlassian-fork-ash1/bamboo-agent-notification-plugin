package com.atlassianlab.bamboo.plugins.agentnotification;

import com.atlassian.bamboo.agent.AgentType;
import com.atlassian.bamboo.event.agent.AgentRegisteredEvent;
import com.atlassian.bamboo.notification.AbstractNotificationType;
import com.atlassian.bamboo.template.TemplateRenderer;
import com.atlassian.bamboo.util.Narrow;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.struts.TextProvider;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import java.util.HashMap;

public class AgentOnlineNotificationType extends AbstractNotificationType {

    private final TemplateRenderer templateRenderer;
    private final TextProvider textProvider;

    @Inject
    public AgentOnlineNotificationType(@ComponentImport TemplateRenderer templateRenderer,
                                       @ComponentImport TextProvider textProvider) {
        this.templateRenderer = templateRenderer;
        this.textProvider = textProvider;
    }

    @Override
    public boolean isNotificationRequired(@NotNull Object event) {
        AgentRegisteredEvent agentRegisteredEvent = Narrow.downTo(event, AgentRegisteredEvent.class);
        return agentRegisteredEvent != null && agentRegisteredEvent.getAgent().getType() != AgentType.ELASTIC;
    }

    @NotNull
    @Override
    public String getEditHtml() {
        final String editTemplateLocation = notificationTypeModuleDescriptor.getEditTemplate();
        return StringUtils.defaultString(templateRenderer.render(editTemplateLocation, new HashMap<>()));
    }

    @NotNull
    public String getViewHtml() {
        return textProvider.getText("agent.notification.online.table.view.remote");
    }
}
