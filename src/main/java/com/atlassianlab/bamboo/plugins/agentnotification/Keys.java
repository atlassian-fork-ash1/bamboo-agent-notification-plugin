package com.atlassianlab.bamboo.plugins.agentnotification;

public interface Keys {
    String ARTIFACT_ID = "com.atlassianlab.bamboo.plugins.bamboo-agent-notification-plugin";
    String ONLINE_NOTIFICATION_PLUGIN_KEY = ARTIFACT_ID + ":agentOnlineNotification";
    String OFFLINE_NOTIFICATION_PLUGIN_KEY = ARTIFACT_ID + ":agentOfflineNotification";
}
