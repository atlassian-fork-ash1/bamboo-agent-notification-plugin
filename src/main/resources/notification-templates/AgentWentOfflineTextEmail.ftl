[#-- @ftlvariable name="i18n" type="com.atlassian.sal.api.message.I18nResolver" --]
[#-- @ftlvariable name="agent" type="com.atlassianlab.bamboo.plugins.agentnotification.Agent" --]
[#include "notificationCommonsText.ftl"][#lt/]
[#assign headline = i18n.getText("agent.notification.offline.title", [agent.name?html]) /][#lt/]
[@notificationTitleText title=headline /]
${i18n.getText("agent.notification.offline.text")} : ${agent.name?html}
[@showEmailFooter /]