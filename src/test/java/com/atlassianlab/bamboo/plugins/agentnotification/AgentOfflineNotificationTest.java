package com.atlassianlab.bamboo.plugins.agentnotification;

import com.atlassian.bamboo.resultsummary.AgentResultsSummaryManager;
import com.atlassian.bamboo.template.TemplateRenderer;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.v2.build.events.AgentOfflineEvent;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class AgentOfflineNotificationTest {
    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock private TemplateRenderer templateRenderer;
    @Mock private AgentResultsSummaryManager agentResultsSummaryManager;
    @InjectMocks private AgentOfflineNotification notification;

    @Test
    public void agentIsAddedForTemplates() {
        final BuildAgent buildAgent = mock(BuildAgent.class);
        notification.setEvent(new AgentOfflineEvent(this, buildAgent));

        notification.getHtmlEmailContent();
        verify(templateRenderer).render(anyString(), argThat(argument -> argument.containsKey(AgentOnlineNotification.CFG_AGENT)));
    }
}