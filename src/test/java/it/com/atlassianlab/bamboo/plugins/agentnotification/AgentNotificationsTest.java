package it.com.atlassianlab.bamboo.plugins.agentnotification;

import com.atlassian.bamboo.pageobjects.BambooTestedProduct;
import com.atlassian.bamboo.pageobjects.BambooTestedProductFactory;
import com.atlassian.bamboo.pageobjects.pages.plan.configuration.notification.ConfigureNotificationComponent;
import com.atlassian.bamboo.testutils.NotificationsTestUtils;
import com.atlassian.bamboo.testutils.TestBuildDetailsBuilder;
import com.atlassian.bamboo.testutils.agent.RemoteAgentInstallation;
import com.atlassian.bamboo.testutils.agent.RemoteAgentProcess;
import com.atlassian.bamboo.testutils.junit.rule.BackdoorRule;
import com.atlassian.bamboo.testutils.model.TestBuildDetails;
import com.atlassian.bamboo.testutils.user.TestUser;
import com.atlassian.bamboo.webdriver.BambooWebDriverTest;
import com.atlassian.bamboo.webdriver.WebDriverTestEnvironmentData;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;
import com.atlassianlab.bamboo.plugins.agentnotification.Keys;
import com.atlassianlab.bamboo.plugins.agentnotification.pageobject.AgentsConfigurationPage;
import com.atlassianlab.bamboo.plugins.agentnotification.pageobject.SystemNotificationsPage;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.icegreen.greenmail.util.GreenMail;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.nio.charset.Charset;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.atlassian.bamboo.testutils.BambooTestConstants.ADMIN_EMAIL;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

public class AgentNotificationsTest extends BambooWebDriverTest {
    private static final Logger log = Logger.getLogger(AgentNotificationsTest.class);
    private final WebDriverTestEnvironmentData environmentData = new WebDriverTestEnvironmentData();
    private final BambooTestedProduct bamboo = BambooTestedProductFactory.create();

    private static final String TEST_LOGIN = "test";
    private static final String TEST_EMAIL = ADMIN_EMAIL;
    private static final String TEST_PASSWORD = TEST_LOGIN;
    private static final long MAX_WAIT_TIME_MSEC = TimeUnit.MINUTES.toMillis(2);

    @Rule public final BackdoorRule backdoor = new BackdoorRule(environmentData);
    @Rule public final WebDriverScreenshotRule screenshotRule = new WebDriverScreenshotRule();

    private GreenMail greenMailServer;

    private RemoteAgentProcess process;
    private RemoteAgentInstallation installation;


    @Before
    public void setUpMailServer() {
        //start mail server and configure it in Bamboo
        greenMailServer = NotificationsTestUtils.setUpGreenMail(TEST_EMAIL, TEST_LOGIN, TEST_PASSWORD);
        NotificationsTestUtils.setUpBambooMailServer(backdoor.asUser(TestUser.ADMIN), greenMailServer, TEST_LOGIN, TEST_PASSWORD);
    }

    @Override
    protected void onAfter() {
        NotificationsTestUtils.tearDownGreenMail(greenMailServer);
        stopAgent();
    }

    private void stopAgent() {
        if (process != null) {
            process.stop();
        }

        if (installation != null) {
            try {
                installation.destroy();
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    /*
     * Test configures mail server, adds both notifications (agent online/offline),
     * starts remote agent, run 2 builds (failed and successful). Check that both emails
     * were sent
     */
    @Test
    public void agentNotificationsAreSentViaEmail() throws Exception {
        bamboo.fastLogin(TestUser.ADMIN);
        //configure notifications for online and offline agents
        bamboo.visit(SystemNotificationsPage.class).addNotification()
                .withCondition(Keys.ONLINE_NOTIFICATION_PLUGIN_KEY)
                .withRecipientType(ConfigureNotificationComponent.RECIPIENT_TYPE_USER)
                .withUserName(TestUser.ADMIN.username)
                .save();

        bamboo.visit(SystemNotificationsPage.class).addNotification()
                .withCondition(Keys.OFFLINE_NOTIFICATION_PLUGIN_KEY)
                .withRecipientType(ConfigureNotificationComponent.RECIPIENT_TYPE_USER)
                .withUserName(TestUser.ADMIN.username)
                .save();
        //disable local agents
        backdoor.agents().disableAllAgents();
        //connect remote agent
        disableRemoteAgentAuthentication();
        connectRemoteAgent();
        //confirm email sent and contains name of remote agent
        final MimeMessage agentOnline = _waitForIncomingEmail(1)[0];
        final String onlineAgentSubject = agentOnline.getSubject();
        assertThat(onlineAgentSubject, containsString("is online"));
        final String cfgGoalsJob1 = "bamboo.variable.testFirstFailedJobNotifications.job.1.goals";
        final String goalsPassing = "exit 0";
        final String goalsFailing = "exit 1";

        final TestBuildDetails build = createFailOnPropertyPlan("testFirstFailedJobNotifications").build();
        backdoor.plans().createPlan(build);
        //run success build on connected remote agent
        backdoor.plans()
                .triggerBuild(build.getKey(), ImmutableMap.of(cfgGoalsJob1, goalsPassing))
                .waitForCompletedBuild(build.getKey(), 1);
        //run failed build on connected remote agent
        backdoor.plans()
                .triggerBuild(build.getKey(), ImmutableMap.of(cfgGoalsJob1, goalsFailing))
                .waitForCompletedBuild(build.getKey(), 2);
        //turn remote agent off
        stopAgent();
        //confirm notification arrived and contains link to agent details and correct icons in build history
        final MimeMessage agentOffline = _waitForIncomingEmail(2)[1];
        assertThat(agentOffline.getSubject(), containsString("offline"));
        final String textBody = getWholeMessage(((MimeMultipart) agentOffline.getContent()).getBodyPart(0));
        assertThat(textBody, containsString("Bamboo has detected an offline agent"));
        final String htmlBody = getWholeMessage(((MimeMultipart) agentOffline.getContent()).getBodyPart(1));
        System.out.println(htmlBody);
        assertThat(htmlBody, containsString("icon-build-failed.png")); // email has icon with failed build
        assertThat(htmlBody, containsString("browse/" + build.getKey() + "-1")); // email has reference to failed build
        assertThat(htmlBody, containsString("icon-build-successful.png")); // email has icon with successful build
        assertThat(htmlBody, containsString("browse/" + build.getKey() + "-2")); // email has refernce to successful build
    }

    private String getWholeMessage(BodyPart msg) throws IOException, MessagingException {
        return String.join("", IOUtils.readLines(msg.getInputStream(), Charset.defaultCharset()));
    }

    private void disableRemoteAgentAuthentication() {
        final AgentsConfigurationPage agentsConfigurationPage = bamboo.visit(AgentsConfigurationPage.class);
        if (!agentsConfigurationPage.isRemoteAgentSupportEnabled()) {
            agentsConfigurationPage.enableRemoteAgentSupport();
        }
        agentsConfigurationPage.disableAgentAuthentication();
    }

    private void connectRemoteAgent() throws Exception {
        installation = new RemoteAgentInstallation(backdoor.asUser(TestUser.ADMIN), environmentData.getBaseUrl().toString());
        process = installation.start();
    }

    private MimeMessage[] _waitForIncomingEmail(int expectedEmailCount) throws Exception {
        if (greenMailServer.getReceivedMessages().length < expectedEmailCount) {
            final List<MimeMessage> mimeMessages = NotificationsTestUtils.waitForIncomingEmail(greenMailServer, Duration.of(MAX_WAIT_TIME_MSEC, ChronoUnit.MILLIS), expectedEmailCount);
            return Iterables.toArray(mimeMessages, MimeMessage.class);
        } else {
            return greenMailServer.getReceivedMessages();
        }
    }

    private TestBuildDetailsBuilder createFailOnPropertyPlan(@NotNull String variableName) {
        return new TestBuildDetailsBuilder()
                .withScriptTask(String.format("${bamboo.%s.job.1.goals}", variableName))
                .withManualBuild()
                .withNoInitialBuild();
    }
}
